package com.delta.cru.rest.poc.mapper;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.delta.cru.rest.poc.vo.EmpVo;

public interface EmployeeMapper {

	@Select("SELECT  E.EMPID,E.LASTNAME,E.FIRSTNAME,E.ADDRESS,E.CITY FROM EMPLOYEE E where E.EMPID=#{employeeid}")
	@Results({ @Result(id = true, column = "EMPL_ID", property = "emplId"),
			@Result(column = "FIRSTNAME", property = "fname"),
			@Result(column = "LASTNAME", property = "lname"),
			@Result(column = "ADDRESS", property = "address"),
			@Result(column = "CITY", property = "city")})

	EmpVo findEmpById(Integer employeeid);

//	@Select("SELECT  EP.EMPL_ID,EP.PH_TYP_CD,EP.EMPL_PH_NB,EP.EFF_DT,EP.EXP_DT FROM EMPL_PH EP ORDER BY EP.EFF_DT DESC,EP.PH_TYP_CD,EP.EMPL_PH_NB")
//	@Results({ @Result(id = true, column = "EMPL_ID", property = "emplId"),
//			@Result(id = true, column = "PH_TYP_CD", property = "phTypCd"),
//			@Result(id = true, column = "EMPL_PH_NB", property = "emplPhNb"),
//			@Result(id = true, column = "EFF_DT", property = "effDt"), @Result(column = "EXP_DT", property = "expDt")
//
//	})
//	List<EmplPhVo> findAllEmpPhns();

}
