/**
 * 
 */
package com.delta.cru.rest.poc.config;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

import com.delta.cru.rest.poc.vo.EmpVo;
import com.delta.cru.rest.poc.vo.EmplPhVo;

/**
 *
 */
@Configuration
@MappedTypes(value = { EmpVo.class, EmplPhVo.class })
@MapperScan("com.delta.cru.rest.poc.mapper")
public class EmployeeConfig {
//	@Bean
//	public DataSource dataSource() throws IllegalArgumentException, NamingException {
//		 JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//		 bean.setJndiName("jdbc/oracle");
//		 bean.setProxyInterface(DataSource.class);
//		 bean.setLookupOnStartup(false);
//		 bean.afterPropertiesSet();
//		 return (DataSource) bean.getObject();
////		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
//////		dataSourceLookup.setResourceRef(false);
////		String dataSourceName = "jdbc/oracle";
////		return dataSourceLookup.getDataSource(dataSourceName);
//
//	}

//	@Bean
//	public SqlSessionFactory sessionFactory() throws Exception {
//		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//		bean.setDataSource(dataSource());
//		return bean.getObject();
//	}

}
