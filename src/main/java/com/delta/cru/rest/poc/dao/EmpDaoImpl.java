/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.delta.cru.rest.poc.mapper.EmployeeMapper;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
@Repository
public class EmpDaoImpl implements EmpDaoIfc {

	@Autowired
	private EmployeeMapper employeeMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.delta.cru.rest.poc.dao.EmpDaoIfc#fEmpById()
	 */
	@Override
	public List<EmpVo> fEmpById() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.delta.cru.rest.poc.dao.EmpDaoIfc#findEmpById(int)
	 */
	@Override
	public EmpVo findEmpById(int empId) {
		return employeeMapper.findEmpById(empId);
	}

}
