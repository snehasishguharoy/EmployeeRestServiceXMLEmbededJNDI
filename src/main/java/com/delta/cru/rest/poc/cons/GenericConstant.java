/**
 * 
 */
package com.delta.cru.rest.poc.cons;

/**
 * @author SNEHASISH GUHA ROY
 *
 */
public class GenericConstant {
	/** The Constant JNDI_NAME. */
	public static final String JNDI_NAME = "jdbc/oracle";

	private GenericConstant() {
		super();
	}
}
