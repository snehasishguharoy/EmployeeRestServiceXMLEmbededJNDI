package com.delta.cru.rest.poc.bo;

import java.util.List;

import com.delta.cru.rest.poc.exp.EmployeeNotFoundException;
import com.delta.cru.rest.poc.vo.EmpVo;

public interface EmpDtlsIfc {
	/**
	 * Find employee by id.
	 *
	 * @return the list
	 */
	List<EmpVo> findEmployeeById()  throws EmployeeNotFoundException;
	
	/**
	 * Find emp by id.
	 *
	 * @param empId the emp id
	 * @return the emp vo
	 */
	EmpVo findEmpById(int empId)  throws EmployeeNotFoundException;


}
