/**
 * 
 */
package com.delta.cru.rest.poc.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.delta.cru.rest.poc.mapper.EmployeeMapper;
import com.delta.cru.rest.poc.vo.EmpVo;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EmpDaoImplTest {
	@InjectMocks
	private EmpDaoImpl daoImpTest;
	@Mock
	private EmployeeMapper employeeMapper;
	@Test
	public void fEmpByIdTest() {

		List<EmpVo> empVos = daoImpTest.fEmpById();
		Assert.assertNotNull(empVos);

	}
	@Test
	public void findEmpByIdTest() {
		EmpVo empVo = daoImpTest.findEmpById(101);
		Assert.assertNull(empVo);

	}

}
